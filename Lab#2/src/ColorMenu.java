import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ColorMenu extends JFrame
	implements ActionListener
{
	public static final int WIDTH = 600;
	public static final int HEIGHT = 300;

	private Container contentPane;

	public ColorMenu()
	{
		setSize(WIDTH, HEIGHT);
		addWindowListener(new WindowDestroyer());
		setTitle("Menu Demonstration");
		contentPane = getContentPane();
		contentPane.setBackground(Color.GRAY);
		contentPane.setLayout(new FlowLayout());

		JMenu fileMenu = new JMenu("File");

		JMenu colorMenu = new JMenu("Color");
		JMenuItem c;

		c = new JMenuItem("Red");
		c.addActionListener(this);
		colorMenu.add(c);

		c = new JMenuItem("Blue");
		c.addActionListener(this);
		colorMenu.add(c);

		c = new JMenuItem("Green");
		c.addActionListener(this);
		colorMenu.add(c);
                
                c = new JMenuItem("Pink");
		c.addActionListener(this);
		colorMenu.add(c);

                c = new JMenuItem("Exit");
		c.addActionListener(this);
		fileMenu.add(c);
                
		JMenuBar mBar = new JMenuBar();
		mBar.add(fileMenu);
		mBar.add(colorMenu);
		setJMenuBar(mBar);
	}

	public void actionPerformed(ActionEvent e)
	{
		String actionCommand = e.getActionCommand();

		if(actionCommand.equals("Red"))
		{
			contentPane.setBackground(Color.RED);
		}
		else if(actionCommand.equals("Blue"))
		{
			contentPane.setBackground(Color.BLUE);
		}
                else if(actionCommand.equals("Green"))
		{
			contentPane.setBackground(Color.GREEN);
		}
                else if(actionCommand.equals("Pink"))
                {
                        contentPane.setBackground(new Color(255,131,250));
                }
                else
                {
                        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
                }
	}

	public static void main(String[] args)
	{
		ColorMenu gui = new ColorMenu();
		gui.setVisible(true);
	}
}