import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ButtonIcons extends JFrame
	implements ActionListener
{
	public static final int WIDTH = 400;
	public static final int HEIGHT = 300;

	private Container contentPane;
	private JPanel buttonPanel;

	public ButtonIcons()
	{
		setSize(WIDTH, HEIGHT);
		addWindowListener(new WindowDestroyer());
		setTitle("Button Icon Demo");
		contentPane = getContentPane();
		contentPane.setBackground(Color.GRAY);
		contentPane.setLayout(new BorderLayout());

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.setBackground(Color.GRAY);

		JButton redButton = new JButton("Red");
		redButton.addActionListener(this);
		buttonPanel.add(redButton);

		JButton blueButton = new JButton("Blue");
		blueButton.addActionListener(this);
		buttonPanel.add(blueButton);

		JButton greenButton = new JButton("Green");
		greenButton.addActionListener(this);
		buttonPanel.add(greenButton);
                
                JButton aquaButton = new JButton("Aqua");
		aquaButton.addActionListener(this);
		buttonPanel.add(aquaButton);

		contentPane.add(buttonPanel, BorderLayout.NORTH);
	}

	public void actionPerformed(ActionEvent e)
	{
		if(e.getActionCommand().equals("Red"))
		{
			contentPane.setBackground(Color.RED);
			buttonPanel.setBackground(Color.RED);
		}
		else if(e.getActionCommand().equals("Blue"))
		{
			contentPane.setBackground(Color.BLUE);
			buttonPanel.setBackground(Color.BLUE);
		}
		else if(e.getActionCommand().equals("Green"))
		{
			contentPane.setBackground(Color.GREEN);
			buttonPanel.setBackground(Color.GREEN);
		}
                else if(e.getActionCommand().equals("Aqua"))
		{
			contentPane.setBackground(new Color(0,245,255));
			buttonPanel.setBackground(new Color(0,245,255));
		}
	}

	public static void main(String[] args)
	{
		ButtonIcons gui = new ButtonIcons();
		gui.setVisible(true);
	}
}