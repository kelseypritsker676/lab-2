import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PanelTest extends JFrame
	implements ActionListener
{
	public static final int WIDTH = 400;
	public static final int HEIGHT = 200;

	public PanelTest()
	{
		setSize(WIDTH,HEIGHT);
		addWindowListener(new WindowDestroyer());
		setTitle("Panel Demonstration");
		Container contentPane = getContentPane();
		contentPane.setBackground(Color.BLUE);
		contentPane.setLayout(new BorderLayout());

		JPanel buttonPanel1 = new JPanel();
		buttonPanel1.setBackground(Color.WHITE);

		buttonPanel1.setLayout(new FlowLayout());

		JButton redButton = new JButton("Red");
		redButton.setBackground(Color.RED);
		redButton.addActionListener(this);
		buttonPanel1.add(redButton);

		JButton greenButton = new JButton("Green");
		greenButton.setBackground(Color.GREEN);
		greenButton.addActionListener(this);
		buttonPanel1.add(greenButton);

		JButton blueButton = new JButton("Blue");
		blueButton.setBackground(Color.BLUE);
		blueButton.addActionListener(this);
		buttonPanel1.add(blueButton);
                
                JButton purpleButton = new JButton("Purple");
		purpleButton.setBackground(new Color(154,50,205));
		purpleButton.addActionListener(this);
		buttonPanel1.add(purpleButton);

		contentPane.add(buttonPanel1, BorderLayout.NORTH);
	}

	public void actionPerformed(ActionEvent e)
	{
		String actionCommand = e.getActionCommand();
		Container contentPane = getContentPane();

		if(actionCommand.equals("Red"))
			contentPane.setBackground(Color.RED);
		else if(actionCommand.equals("Green"))
			contentPane.setBackground(Color.GREEN);
		else if(actionCommand.equals("Blue"))
			contentPane.setBackground(Color.BLUE);
                else if(actionCommand.equals("Purple"))
			contentPane.setBackground(new Color(154,50,205));
	}

	public static void main(String[] args)
	{
		PanelTest gui = new PanelTest();
		gui.setVisible(true);
	}
}