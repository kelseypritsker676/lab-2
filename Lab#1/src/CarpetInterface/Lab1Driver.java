/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CarpetInterface;
import java.util.*;
/**
 *
 * @author kelsey.pritsker676
 */
public class Lab1Driver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double length;
        double width;
        double cost;
        RoomDimension dim;
        RoomCarpet room;
        
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Please Enter Length of Room:");
        length = scan.nextDouble();
        System.out.println("Please Enter Width of Room:");
        width = scan.nextDouble();
        dim = new RoomDimension(length,width);
        System.out.println("Please Enter Cost Per Sqrft:");
        cost = scan.nextDouble();
        room = new RoomCarpet(dim, cost);
        System.out.println(room.toString());
    }
}
